#!/bin/bash

# steamvrlite.sh
# Author: NEON725
# Uses black magic to launch steamvr in a way that bypasses most of the UI.
# By directly launching the vrstartup command, the VR backend, API, and drivers are loaded, but not the main window for accessing settings etc.
# On my system, this caused SteamVR to not spawn a dozen chromium instances, improving performance significantly.
# Currently, though vrdashboard processes are spawned, the dashboard is not working in-game. This was left alone since it causes negligable performance losses.
# Usage: Customize the variables below, then launch from a terminal or configure SteamVR's launch parameters to call this script with %command% as the only argument.
# If GUI mode is selected, this script will call the passed arguments as a command with no alteration. For this to be useful, you must launch this script from steam, providing the %command% keyword as the script argument.
# NOTE: This script does not automatically launch a terminal, and without one, you cannot interact with the prompt. SteamVR Lite will still launch, but you will not be able to choose GUI mode, and shutting down the system will be more difficult.
# Example Launch Parameters: xfce4-terminal -x /path/to/script/steamvrlite.sh %command%

# Default values assume debian/ubuntu and the default SteamLibrary installation for SteamVR.
export _STEAM_LIBRARY="$HOME/.steam/debian-installation"
export _STEAM_RUNTIME_DIR="$_STEAM_LIBRARY/ubuntu12_32/steam-runtime"
# If it complains about missing .so files, find them and add the folder here, colon-separated.
export _ADDL_LIBRARY_PATHS=""

# This method of launching SteamVR unreliably cleans up background processes, so we take responsibility for ending them.
function cleanup {
	echo "Cleanup started."
	# Allow processes to exit gracefully, then insist.
	sleep 10s
	echo "Killing processes."
	killall vrwebhelper.sh
	killall vrdashboard
	killall vrcompositor
	killall vrmonitor
	# Sometimes the dashboard is persistant.
	sleep 5s
	echo "Finished."
	killall -s 9 vrdashboard
	exit
}
trap cleanup EXIT

echo 'Called with:' $@
echo "Press a letter key to launch GUI mode, or wait to launch lite mode."
read -t 10 -n 1 input

if [[ "$input" == "" ]]
then
	echo "Launching lite mode..."
	cd "$_STEAM_LIBRARY/steamapps/common/SteamVR/bin/linux64"
	# SteamVR requires the steam runtime to launch.
	export LD_LIBRARY_PATH="$PWD:$_STEAM_RUNTIME_DIR/lib/x86_64-linux-gnu"
	if [[ "$_ADDL_LIBRARY_PATHS" != "" ]]
	then
		export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${_ADDL_LIBRARY_PATHS}"
	fi
	./vrstartup
else
	echo "Launching GUI mode..."
	# "GUI Mode" assumes that we were called from steam, and we launch whatever command steam wants us to.
	$@
fi

# NEON SteamVR Tricks

A collection of terminal helpers that are designed to make SteamVR a nicer experience on Linux.
See individual scripts for usage information.
